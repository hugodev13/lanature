//Noms de classe globales provenant de Box2d
var b2Vec2 = Box2D.Common.Math.b2Vec2
    , b2AABB = Box2D.Collision.b2AABB
    , b2BodyDef = Box2D.Dynamics.b2BodyDef
    , b2Body = Box2D.Dynamics.b2Body
    , b2FixtureDef = Box2D.Dynamics.b2FixtureDef
    , b2Fixture = Box2D.Dynamics.b2Fixture
    , b2World = Box2D.Dynamics.b2World
    , b2MassData = Box2D.Collision.Shapes.b2MassData
    , b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape
    , b2CircleShape = Box2D.Collision.Shapes.b2CircleShape
    , b2DebugDraw = Box2D.Dynamics.b2DebugDraw
    , b2Shape = Box2D.Collision.Shapes.b2Shape
    , b2Joint = Box2D.Dynamics.Joints.b2Joint
    , b2Settings = Box2D.Common.b2Settings
    ;

var world;
var ctx;
var canvas_width;
var canvas_height;

//Echelle : 1 metre = 105px de canvas
var scale = 105;


//Cette fonction est appelée dans une boucle pour redessiner le monde
function draw_world(world, context) {
    //Efface (clean) le canvas
    ctx.clearRect( 0 , 0 , canvas_width, canvas_height );

    ctx.fillStyle = '#4a3d28';
    ctx.fillRect(0,0, canvas_width, canvas_height);

    //Convertit les coordonnées du canvas en cartesiennes
    ctx.save();
    ctx.translate(0 , canvas_height);
    ctx.scale(1 , -1);
    world.DrawDebugData();
    ctx.restore();
}

//Creation du monde
function createWorld() {
    //Gravité : x = 0 , y = -10 m/s² (Terre)
    var gravity = new b2Vec2(0, -10);

    world = new b2World(gravity , true);

    //Installe debugDraw
    var debugDraw = new b2DebugDraw();
    debugDraw.SetSprite(document.getElementById("canvas").getContext("2d"));
    debugDraw.SetDrawScale(scale);
	debugDraw.SetFillAlpha(1);	//Transparence des elements entre 0 et 1
    debugDraw.SetLineThickness(1.0);	// Epaisseur des traits
    	debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);

	world.SetDebugDraw(debugDraw);
	
	//Creation du mur gauche
	createBox(world, -0.03, 1, 0.02, 5, {type : b2Body.b2_staticBody});
	//Creation du mur droit
	createBox(world, 9.55, 1, 0.02, 5, {type : b2Body.b2_staticBody});
	//Creation de la plateforme pour la balle
	createBox(world, 0.8, 4, 0.8, 0.01, {type : b2Body.b2_staticBody});
	
	//Creation de la zone finale
	left_dough = createBox(world, 6, 0.35, 0.04, 0.25, {type : b2Body.b2_staticBody});
	right_dough = createBox(world, 5.3, 0.35, 0.04, 0.25, {type : b2Body.b2_staticBody});
	belly = createBox(world, 5.6, 0.85, 0.6, 0.3, {type : b2Body.b2_staticBody});
	head = createBox(world, 4.9, 0.9, 0.14, 0.135, {type : b2Body.b2_staticBody});
	tail = createBox(world, 6.3, 0.85, 0.16, 0.03, {type : b2Body.b2_staticBody});
	
	//Deplacement de la zone finale
	barGauche = left_dough.GetPosition();
	barSol = right_dough.GetPosition();
	barDroite = belly.GetPosition();
	barHautGauche = head.GetPosition();
	barHautDroite = tail.GetPosition();
	
	
	window.onkeydown = function(event) {
		
        if(event.key == 'ArrowLeft') { 
			barGauche.x -= 0.14;
			barSol.x -= 0.14;
			barDroite.x -= 0.14;
			barHautGauche.x -= 0.14;
			barHautDroite.x -= 0.14;
			left_dough.SetPosition(barGauche);
			right_dough.SetPosition(barSol);
			belly.SetPosition(barDroite);
			head.SetPosition(barHautGauche);
			tail.SetPosition(barHautDroite);
		}	
		
        if(event.key == 'ArrowRight') { 
			barGauche.x += 0.14;
			barSol.x += 0.14;
			barDroite.x += 0.14;
			barHautGauche.x += 0.14;
			barHautDroite.x += 0.14;
			left_dough.SetPosition(barGauche);
			right_dough.SetPosition(barSol);
			belly.SetPosition(barDroite);
			head.SetPosition(barHautGauche);
			tail.SetPosition(barHautDroite);
		}	
		
        if(event.keyCode == 32) { 
            barGauche.y += 0.14;
            barSol.y += 0.14;
            barDroite.y += 0.14;
            barHautGauche.y += 0.14;
            barHautDroite.y += 0.14;
            left_dough.SetPosition(barGauche);
            right_dough.SetPosition(barSol);
            belly.SetPosition(barDroite);
            head.SetPosition(barHautGauche);
            tail.SetPosition(barHautDroite);
        }
	};
    
    return world;
}

//Creation des sols
function createGround(world) {
    var bodyDef = new b2BodyDef();

    var fixDef = new b2FixtureDef();
    fixDef.density = 1.0;
    fixDef.friction = 1.0;
    fixDef.restitution = 0.5;

    fixDef.shape = new b2PolygonShape;

    //Mention de la moitié des tailles
    fixDef.shape.SetAsBox(4.00 , .5);

    //Regler la position du centre
    bodyDef.position.Set(4.10 , 1);

    return world.CreateBody(bodyDef).CreateFixture(fixDef);
}

//Creation de balle
function createBall(world, x, y, r, options) {
    var body_def = new b2BodyDef();
    var fix_def = new b2FixtureDef;

    fix_def.density = 1;
    fix_def.friction = 1;
    fix_def.restitution = 0.99;

    var shape = new b2CircleShape(r);
    fix_def.shape = shape;

    body_def.position.Set(x , y);

    body_def.linearDamping = 0.0;
    body_def.angularDamping = 0.0;
    
    body_def.type = b2Body.b2_dynamicBody;

    body_def.userData = options.user_data;
    
    var b = world.CreateBody( body_def );
    b.CreateFixture(fix_def);
    
    return b;
}

//Creation d'element quelconque (maintenant la balle)
function createHelloWorld() {

    balle = createBall(world, 1 , 4.26, 0.2, {'user_data' : {'drawImage' : 'img, 10, 10'}});
    
    posBalle = balle.GetPosition();	//Coordonnées de la balle
}

//Creation de cube non soumis à la gravité
function createBox(world, x, y, width, height, options) {
     //Parametre par defaut
    options = $.extend(true, {
        'density' : 1.2 ,
        'friction' : 1.0,
        'restitution' : 0.5 ,

        'linearDamping' : 0.0 ,
        'angularDamping' : 0.0 ,

        'type' : b2Body.b2_staticBody
    }, options);

    var body_def = new b2BodyDef();
    var fix_def = new b2FixtureDef();

    fix_def.density = options.density;
    fix_def.friction = options.friction;
    fix_def.restitution = options.restitution;

    fix_def.shape = new b2PolygonShape();

    fix_def.shape.SetAsBox( width , height );

    body_def.position.Set(x , y);

    body_def.linearDamping = options.linearDamping;
    body_def.angularDamping = options.angularDamping;

    body_def.type = options.type;
    body_def.userData = options.user_data;

    var b = world.CreateBody( body_def );
    var f = b.CreateFixture(fix_def);

    return b;
}


//Creation de cube soumis à la gravité
function createBoxG(world, x, y, width, height, options) {
     //Parametre par defaut
    options = $.extend(true, {
        'density' : 1.2 ,
        'friction' : 1.0,
        'restitution' : 0.5 ,

        'linearDamping' : 0.0 ,
        'angularDamping' : 0.0 ,

        'type' : b2Body.b2_dynamicBody
    }, options);

    var body_def = new b2BodyDef();
    var fix_def = new b2FixtureDef();

    fix_def.density = options.density;
    fix_def.friction = options.friction;
    fix_def.restitution = options.restitution;

    fix_def.shape = new b2PolygonShape();

    fix_def.shape.SetAsBox( width , height );

    body_def.position.Set(x , y);

    body_def.linearDamping = options.linearDamping;
    body_def.angularDamping = options.angularDamping;

    body_def.type = options.type;
    body_def.userData = options.user_data;

    var b = world.CreateBody( body_def );
    var f = b.CreateFixture(fix_def);

    return b;
}

//Creation du couvercle
function createCouvercle(world, x, y, width, height, options) {
     //Parametre par defaut
    options = $.extend(true, {
        'density' : 1000,
        'friction' : 40,
        'restitution' : 0.1,

        'linearDamping' : 0.0 ,
        'angularDamping' : 0.0 ,

        'type' : b2Body.b2_dynamicBody
    }, options);

    var body_def = new b2BodyDef();
    var fix_def = new b2FixtureDef();

    fix_def.density = options.density;
    fix_def.friction = options.friction;
    fix_def.restitution = options.restitution;

    fix_def.shape = new b2PolygonShape();

    fix_def.shape.SetAsBox( width , height );

    body_def.position.Set(x , y);

    body_def.linearDamping = options.linearDamping;
    body_def.angularDamping = options.angularDamping;

    body_def.type = options.type;
    body_def.userData = options.user_data;

    var b = world.CreateBody( body_def );
    var f = b.CreateFixture(fix_def);

    return b;
}

//Creation de poids
function createPoids(world, x, y, width, height, options) {
     //Parametre par defaut
    options = $.extend(true, {
        'density' : 6,
        'friction' : 5.0,
        'restitution' : 1.2,

        'linearDamping' : 0.0 ,
        'angularDamping' : 0.0 ,

        'type' : b2Body.b2_dynamicBody
    }, options);

    var body_def = new b2BodyDef();
    var fix_def = new b2FixtureDef();

    fix_def.density = options.density;
    fix_def.friction = options.friction;
    fix_def.restitution = options.restitution;

    fix_def.shape = new b2PolygonShape();

    fix_def.shape.SetAsBox( width , height );

    body_def.position.Set(x , y);

    body_def.linearDamping = options.linearDamping;
    body_def.angularDamping = options.angularDamping;

    body_def.type = options.type;
    body_def.userData = options.user_data;

    var b = world.CreateBody( body_def );
    var f = b.CreateFixture(fix_def);

    return b;
}

//Fonction qui dessine le monde en boucle
function step() {
    var fps = 60;
    var timeStep = 1.0/fps;

    //move the world ahead , step ahead man!!
    world.Step(timeStep , 8 , 3);
    world.ClearForces();

    draw_world(world, ctx);
}

//Convertion des coordonnées en canvas au monde box2d
function get_real(p)
{
    return new b2Vec2(p.x + 0, 6 - p.y);
}

/* bouton apres defaite ou victoire */
function rejouer() {
	location.reload();
}

/* bouton jouer */
function jouer() {
	
	/* Desactive le bouton "jouer" */
	var bouton_jouer = document.getElementById('jouer');
	bouton_jouer.setAttribute('disabled', 'true');

	jouer_crazy();
}



//Fonction principale (bouton "jouer crazy")
function jouer_crazy() {

	/* Desactive le bouton "jouer" */
	var bouton_jouer = document.getElementById('jouer');
	bouton_jouer.setAttribute('disabled', 'true');
		
	
	/* creer le canvas avec ses attributs */
	var canvas = document.createElement('canvas');
	canvas.setAttribute("width", "1000");
	canvas.setAttribute("height", "600");
	canvas.setAttribute("id", "canvas");
	canvas.setAttribute("position", "relative");
	document.getElementById('jeu').appendChild(canvas);
	
	
	/* creation du choix de l'element Rectangle */
	var choix_un = document.createElement('input');
	choix_un.setAttribute("type", "radio");
	choix_un.setAttribute("class", "choix_radio");
	choix_un.setAttribute("id", "un");
	choix_un.setAttribute("name", "groupe");
	document.getElementById('choixDuRectangle').appendChild(choix_un);
	
	var mot_un = document.createElement('label');
	mot_un.setAttribute("for", "un");
	mot_un.setAttribute("id", "mot_un");
	mot_un.setAttribute("display", "inline-block");
	document.getElementById('choixDuRectangle').appendChild(mot_un);
	document.getElementById('mot_un').innerHTML = "Rectangle";
	
	
	/* Creation du nombre de rectangle à poser --> 2 (facile) */
	var nRectangle = document.createElement('p');
	nRectangle.setAttribute("class", "nombre");
	nRectangle.setAttribute("id", "nombre_trois");
	document.getElementById('nombreDeRectangle').appendChild(nRectangle);
	var nRect = 5;
	document.getElementById('nombre_trois').innerHTML = nRect;
	
	
	/* creation du choix de l'element Carre */
	var choix_deux = document.createElement('input');
	choix_deux.setAttribute("type", "radio");
	choix_deux.setAttribute("class", "choix_radio");
	choix_deux.setAttribute("id", "deux");
	choix_deux.setAttribute("name", "groupe");
	document.getElementById('choixDuCarre').appendChild(choix_deux);
	
	var mot_deux = document.createElement('label');
	mot_deux.setAttribute("for", "deux");
	mot_deux.setAttribute("id", "mot_deux");
	document.getElementById('choixDuCarre').appendChild(mot_deux);
	document.getElementById('mot_deux').innerHTML = "Carré";
	
	
	/* Creation du nombre de carre à poser --> 3 (facile)*/
	var nCarre = document.createElement('p');
	nCarre.setAttribute("class", "nombre");
	nCarre.setAttribute("id", "nombre_quatre");
	document.getElementById('nombreDeCarre').appendChild(nCarre);
	var nCa = 6;
	document.getElementById('nombre_quatre').innerHTML = nCa;
	
	/* creation du choix de l'element Couvercle */
	var choix_trois = document.createElement('input');
	choix_trois.setAttribute("type", "radio");
	choix_trois.setAttribute("class", "choix_radio");
	choix_trois.setAttribute("id", "trois");
	choix_trois.setAttribute("name", "groupe");
	document.getElementById('choixDuCouvercle').appendChild(choix_trois);
	
	var mot_trois = document.createElement('label');
	mot_trois.setAttribute("for", "trois");
	mot_trois.setAttribute("id", "mot_trois");
	document.getElementById('choixDuCouvercle').appendChild(mot_trois);
	document.getElementById('mot_trois').innerHTML = "Branche";
	
	
	/* Creation du nombre de couvercle à poser --> 1 (facile) */
	var nCouvercle = document.createElement('p');
	nCouvercle.setAttribute("class", "nombre");
	nCouvercle.setAttribute("id", "nombre_deux");
	document.getElementById('nombreDeCouvercle').appendChild(nCouvercle);
	var nCou = 5;
	document.getElementById('nombre_deux').innerHTML = nCou;
	
	/* creation du choix de l'element Poids */
	var choix_quatre = document.createElement('input');
	choix_quatre.setAttribute("type", "radio");
	choix_quatre.setAttribute("class", "choix_radio");
	choix_quatre.setAttribute("id", "quatre");
	choix_quatre.setAttribute("name", "groupe");
	document.getElementById('choixDuPoids').appendChild(choix_quatre);
	
	var mot_quatre = document.createElement('label');
	mot_quatre.setAttribute("for", "quatre");
	mot_quatre.setAttribute("id", "mot_quatre");
	document.getElementById('choixDuPoids').appendChild(mot_quatre);
	document.getElementById('mot_quatre').innerHTML = "Goutte d'eau";
	
	
	/* Creation du nombre de poids à poser --> 5 (facile) */
	var nPoids = document.createElement('p');
	nPoids.setAttribute("class", "nombre");
	nPoids.setAttribute("id", "nombre_un");
	document.getElementById('nombreDePoids').appendChild(nPoids);
	var nP = 10;
	document.getElementById('nombre_un').innerHTML = nP;
	
	var canvas = $('#canvas');
    	ctx = canvas.get(0).getContext('2d');
    
    

    //Creation du monde
    world = createWorld();
    
    /* Creation du sol  (world, x, y, widht, height, option) */
    createBox(world, 1, 0, 10, 0.12, {type : b2Body.b2_staticBody});
    createBox(world, 2.2, 0, 0.3, 0.15, {type : b2Body.b2_staticBody});
    createBox(world, 4, 0, 0.6, 0.13, {type : b2Body.b2_staticBody});
    createBox(world, 6.5, 0, 0.6, 0.13, {type : b2Body.b2_staticBody});

    //Obtention des dimensions internes du canvas
    canvas_width = parseInt(canvas.attr('width'));
    canvas_height = parseInt(canvas.attr('height'));

    //Appel de la fonction : Creation d'element quelconque
    createHelloWorld();

    //click event handler on our world
    canvas.click( function(e)
    {
        var p = get_real(new b2Vec2((e.clientX - 5)/scale, (e.clientY - 60)/scale));

       //Creation des formes
       if (choix_un.checked == true && nRect > 0)
       {
       		createBoxG(world, p.x , p.y , .6 , .2);
       		nRect--;
       		document.getElementById('nombre_trois').innerHTML = nRect;
       	}
       	
       	if (choix_deux.checked == true && nCa > 0)
       	{
       		createBoxG(world, p.x , p.y , .25 , .25);
       		nCa--;
       		document.getElementById('nombre_quatre').innerHTML = nCa;
       	}
       	
       	if (choix_trois.checked == true && nCou > 0)
       	{
       		createCouvercle(world, p.x , p.y , .9 , 0.07);
       		nCou--;
       		document.getElementById('nombre_deux').innerHTML = nCou;
       	}
       	
       	if (choix_quatre.checked == true && nP > 0)
       	{
       		createPoids(world, p.x , p.y , 0.03 , 0.03);
       		nP--;
       		document.getElementById('nombre_un').innerHTML = nP;
       	}
       		
       
  
    });

	
	/* Calcul de la victoire et de la défaite */
	var calcul = setInterval(perdu, 100);
	var calcul2 = setInterval(gagne, 7300);
	var calcul3 = setInterval(poidsQuiTombent, 3500);
	
	function perdu() {
		
		if (posBalle.y < -30) {
			$('body').empty();
			
			var affichage_perdu = document.createElement('div');
			affichage_perdu.setAttribute("class", "perdu");
			affichage_perdu.setAttribute("id", "affichage");
			document.getElementById('noeud').appendChild(affichage_perdu);
			document.getElementById('affichage').innerHTML = "Game Over";
		
			
			var rejouer = document.createElement('button');
			rejouer.setAttribute("onclick", "rejouer()");
			rejouer.setAttribute("id", "rejouer");
			document.getElementById('noeud').appendChild(rejouer);
			document.getElementById('rejouer').innerHTML = "Rejouer";
			

			
			clearInterval(calcul);
		}
	}
	
	function gagne() {
		
		if (posBalle.x > barGauche.x && posBalle.x < barDroite.x && posBalle.y > barSol.y && posBalle.y < 2.35) {
			$('body').empty();
			
			var affichage_gagne = document.createElement('div');
			affichage_gagne.setAttribute("class", "gagne");
			affichage_gagne.setAttribute("id", "affichage_deux");
			document.getElementById('noeud').appendChild(affichage_gagne);
			document.getElementById('affichage_deux').innerHTML = "Gagné !";
			
	
			
			var rejouer = document.createElement('button');
			rejouer.setAttribute("onclick", "rejouer()");
			rejouer.setAttribute("id", "rejouer");
			document.getElementById('noeud').appendChild(rejouer);
			document.getElementById('rejouer').innerHTML = "Rejouer";
			
			
			clearInterval(calcul);
		}
	}
	
	
	
	setInterval(function(){
			barGauche.x -= 0.05;
			barSol.x -= 0.05;
			barDroite.x -= 0.05;
			barHautGauche.x -= 0.05;
			barHautDroite.x -= 0.05;
			left_dough.SetPosition(barGauche);
			right_dough.SetPosition(barSol);
			belly.SetPosition(barDroite);
			head.SetPosition(barHautGauche);
			tail.SetPosition(barHautDroite);
	}, 300);
	
	
	function poidsQuiTombent() {
		createPoids(world, 2 , 5.9 , 0.03 , 0.03);
		createPoids(world, 4 , 5.9 , 0.03 , 0.03);
		createPoids(world, 6 , 5.9 , 0.03 , 0.03);
		createPoids(world, 8 , 5.9 , 0.03 , 0.03);
	}
	
     window.setInterval(step, 1000 / 60);
};
